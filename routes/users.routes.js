const router = require('express').Router()
const controller = require('../controllers/users.controllers')

router.get('/get_user_and_posts/:user_id',controller.get_user_and_posts)
router.get('/create_users',controller.create_users)

module.exports = router