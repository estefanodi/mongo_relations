const router = require('express').Router()
const controller = require('../controllers/posts.controllers')

router.post('/create',controller.create)
router.get('/get_single_post/:post_id',controller.get_single_post)
router.get('/get_all_posts',controller.get_all_posts)

module.exports = router 