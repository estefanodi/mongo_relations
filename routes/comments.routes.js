const router = require('express').Router()
const controller = require('../controllers/comments.controllers')

router.post('/create',controller.create)
router.post('/get_all_posts',controller.get_all_posts)


module.exports = router