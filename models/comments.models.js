const mongoose = require('mongoose')

const commentSchema = new mongoose.Schema({
   comment: {
       type: String
   },
   postId: {
    type:mongoose.Schema.Types.ObjectId,
    ref: 'posts'
   },
   createdBy: {
    type:mongoose.Schema.Types.ObjectId
   }
});

module.exports = mongoose.model('comments', commentSchema)