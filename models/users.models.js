const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
   username: String,
   _id: {type:mongoose.Schema.Types.ObjectId}
});

userSchema.virtual('get_related_posts', {// ==> name of the virtual
   ref: 'posts',                         // ==> name of the collection 
   localField: '_id',
   foreignField: 'createdByUserId'
});
userSchema.set('toObject', { virtuals: true });
userSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('users', userSchema)

