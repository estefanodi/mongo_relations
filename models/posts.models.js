const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
   title: {
       type: String
   },
   content: {
       type: String
   },
   createdBy: {
       type:mongoose.Schema.Types.ObjectId,
       ref: 'users'
   } 
});

postSchema.virtual('get_related_comments', {
    ref: 'comments',
    localField: '_id',
    foreignField: 'postId'
});
postSchema.set('toObject', { virtuals: true });
postSchema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('posts', postSchema)

