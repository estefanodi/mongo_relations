    const express  = require('express')
	const app      = express()
	const mongoose = require('mongoose')
	const port     = process.env.PORT || 8080
	const cors     = require('cors') 
	//==========================================================================
	const bodyParser = require('body-parser')
	app.use(bodyParser.urlencoded({extended: true}))
	app.use(bodyParser.json())
	//==========================================================================
	//=============== CHANGE THE NAME OF THE DATABASE ==========================
	//==========================================================================
	// connecting to mongo and checking if DB is running
	async function connecting(){
		try {
			await mongoose.connect('mongodb://127.0.0.1/blog', { useUnifiedTopology: true , useNewUrlParser: true })
			console.log('Connected to the DB')
		} catch ( error ) {
			console.log('ERROR: Seems like your DB is not running, please start it up !!!');
		}
	}
	connecting()
	// temp stuff to suppress internal warning of mongoose which would be updated by them soon
	mongoose.set('useCreateIndex', true);
	// end of connecting to mongo and checking if DB is running
	//==========================================================================
	app.use(cors())
	//==========================================================================
	app.use('/posts',require('./routes/posts.routes.js'))
	app.use('/comments',require('./routes/comments.routes.js'))
	app.use('/users',require('./routes/users.routes.js'))
	//==========================================================================
	app.listen(port,()=>{
	    console.log('server running on port : ' +  port)
	})