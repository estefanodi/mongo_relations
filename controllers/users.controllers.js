const Users = require('../models/users.models');
const { ObjectId } = require('mongoose').Types;

const users = Array(
	{ _id : "5dfb4b1b9fd312209e45c305", username : "Mario" },
	{ _id : "5dfb4b1b9fd312209e45c306", username : "Anna" },
	{ _id : "5dfb4b1b9fd312209e45c307", username : "Andrea" },
	{ _id : "5dfb4b1b9fd312209e45c308", username : "Lucia" }
)

const create_users = (req,res) => {
	try{
		const created = []
        users.forEach( async item => {
			const user = await Users.create({
				username: item.username,
                _id: new ObjectId(item._id)
			})
			created.push(user)
		})
		res.json({ok:true,created})
	}catch( error ){
		res.json({ok:false,message:'Something went wrong',error})
	}
}

const get_user_and_posts = async (req,res) => {
	const { user_id } = req.params
	try{
        const user = await Users.find({_id:user_id}).populate('posts')
        res.json({ok:true,message:'User found',user})
	}catch( error ){
		res.json({ok:false,message:'Something went wrong',error})
	}
}

module.exports = {
	get_user_and_posts,
	create_users
}