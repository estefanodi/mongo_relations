const Comments = require('../models/comments.models');
const Posts = require('../models/posts.models');
const { ObjectId } = require('mongoose').Types;

const users = Array(
	{ _id : "5dfb4b1b9fd312209e45c305", username : "Mario" },
	{ _id : "5dfb4b1b9fd312209e45c306", username : "Anna" },
	{ _id : "5dfb4b1b9fd312209e45c307", username : "Andrea" },
	{ _id : "5dfb4b1b9fd312209e45c308", username : "Lucia" }
)

const posts = Array()

const get_all_posts = async(req,res) => {
   try{
       const response = await Posts.find({})
       response.forEach( post => posts.push(post))
       res.json({ok:true,posts})
   }catch(error){
       res.json({ok:false,message:'Something went wrong'})    
   }
}

const create = async (req,res) => {
	const randomUser = users[Math.floor(Math.random()*users.length)];
	const randomPost = posts[Math.floor(Math.random()*posts.length)];
	const randomNumber = Math.floor((Math.random() * 1000000) + 1);
	try{
		const comment = await Comments.create({
                                                comment: `Comment number: ${randomNumber}, created by ${randomUser.username}`,
                                                postId: new ObjectId(randomPost._id),
                                                createdBy: new ObjectId(randomUser._id)
		})
		res.json({ok:true,comment})
	}catch( error ){
		res.json({ok:false,error:error})
	}
}

module.exports = { 
        create,
        get_all_posts
}