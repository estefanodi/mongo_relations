const Posts = require('../models/posts.models');
const { ObjectId } = require('mongoose').Types;

const users = Array(
	{ _id : "5dfb4b1b9fd312209e45c305", username : "Mario" },
	{ _id : "5dfb4b1b9fd312209e45c306", username : "Anna" },
	{ _id : "5dfb4b1b9fd312209e45c307", username : "Andrea" },
	{ _id : "5dfb4b1b9fd312209e45c308", username : "Lucia" }
)

const create = async (req,res) => {
	const randomUser = users[Math.floor(Math.random()*users.length)];
	const randomNumber = Math.floor((Math.random() * 1000000) + 1);
	try{
		const post = await Posts.create({
							title: `Post number: ${randomNumber}, created by ${randomUser.username}`,
							content: `This is the content of ${randomUser.username}'s post`,
							createdBy: new ObjectId(randomUser._id)
		})
		res.json({ok:true,post})
	}catch( error ){
		res.json({ok:false,error:error})
	}
}

const get_single_post = async (req,res) => {
	const { post_id } = req.params
	try{
		const post = await Posts.find({_id:post_id}).populate('get_related_comments')
		res.json({ok:true,post})
	}catch( error ){
		res.json({ok:false,error:error})
	}
}
     
const get_all_posts = async (req,res) => {
	try{
		const post = await Posts.find({}).populate('get_related_comments')
		res.json({ok:true,post})
	}catch( error ){
		res.json({ok:false,error:error})
	}
}

module.exports = { 
	create,
	get_single_post,
	get_all_posts
}